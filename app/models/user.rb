class User < ApplicationRecord
    has_many :memes, inverse_of: :author
    has_many :follows, inverse_of: :user
    has_many :followers, through: :follows
    has_many :followings, class_name: "Follow", foreign_key: :follow_user_id, inverse_of: :follower
    has_many :users_i_follow, through: :followings, source: :user

    # Memes
    has_many :starred, class_name: "Star", foreign_key: :user_id, inverse_of: :user
end
