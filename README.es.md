# Picomeme
¡Cuando el Fediverso y los memes se encuentran!  
Picomeme es una red social basada en [ActivityPub](https://activitypub.rocks), donde puedes ver memes de todo tipo. De momento, solo está en inglés y español, pero
se agradece que alguien lo traduzca a otros lenguajes.
