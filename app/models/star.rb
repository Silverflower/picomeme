class Star < ApplicationRecord
  belongs_to :user, inverse_of: :starred
  belongs_to :meme, inverse_of: :stars
end
