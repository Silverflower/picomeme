class Follow < ApplicationRecord
  belongs_to :user, inverse_of: :follows
  belongs_to :follower, inverse_of: :followers, class_name: 'User', foreign_key: :follow_user_id
end
