# Picomeme
When Fediverse meets memes!  
Picomeme is an [ActivityPub](https://activitypub.rocks)-based social network focused on
meme sharing. You can view any class of memes here. By the moment, is only in English and Spanish,
but I thank any translation to other languages!
_Note: I'm not a native English Speaker. my English is decent, but not perfect. My native language is Spanish_
