class Meme < ApplicationRecord
    belongs_to :author, class_name: "User", inverse_of: :memes
    has_one_attached :media
    has_many :stars, inverse_of: :meme
end
